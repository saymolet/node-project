#!/usr/bin/env groovy

// @Library('jenkins-shared-library')_
// // https://gitlab.com/saymolet/jenkins-shared-library

library identifier: 'jenkins-shared-library@main', retriever: modernSCM (
    [$class: 'GitSCMSource',
      remote: 'https://gitlab.com/saymolet/jenkins-shared-library.git',
      credentialsId: 'gitlab-credentials'
    ]
)

pipeline {
    agent any

    // Install NodeJS plugin in Jenkins
    tools {
        nodejs "node"
    }

    stages {
        stage("test") {
            steps {
                script {
                    dir("app") {
                        // runt test. If test fails abort build
                        sh 'npm install'
                        sh 'npm run test'
                    }
                }
            }
        }

        stage("increment_version") {
            steps {
                script {
                    dir("app") {
                        // increment version in package.json. major 1.x.x, minor x.1.x, patch x.x.1
                        sh "npm version minor"

                        // save image name. "readJSON" came from Pipeline Utility Steps plugin in Jenkins
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version
                        def app_name = packageJson.name

//                         def version = sh (returnStdout: true, script: "grep 'version' package.json | cut -d '\"' -f4 | tr '\\n' '\\0'")
//                         def app_name = sh (returnStdout: true, script: "grep 'name' package.json | cut -d '\"' -f4 | tr '\\n' '\\0'")
//                         either way works

                        env.IMAGE_NAME = "$app_name-$version-$BUILD_NUMBER"
                    }
                }
            }
        }

        stage("build and push docker image") {
            steps {
                script {
                    buildImage "saymolet/my-repo:$IMAGE_NAME"
                    // credentials id from Jenkins
                    dockerLogin "dockerhub-credentials"
                    dockerPush "saymolet/my-repo:$IMAGE_NAME"
                }
            }
        }

        stage ("deploy to ec2") {
            steps {
                script {
                    echo "Deploying to EC2 instance"
                    // direct docker command
//                     def dockerCmd = "docker run -d -p 3000:3000 saymolet/my-repo:$IMAGE_NAME"

                    // ssh agent plugin in Jenkins
                    // ec2-server-key - credential in Jenkins
//                     def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"

                    // shell commands
                    // passing newest image name to shell script
                    def shellCmd = "bash ./server-commands.sh saymolet/my-repo:${IMAGE_NAME}"

                    def ec2_instance = "ec2-user@35.158.120.145"

                    sshagent(['ec2-server-key']) {
                        // Run docker commands directly on ec2 server
//                         sh "ssh -o StrictHostKeyChecking=no ${ec2_instance} ${dockerCmd}"

                        // copy docker compose file with secure copy to ec2 server
//                         sh "scp docker-compose.yaml ${ec2_instance}:/home/ec2-user"
//                         sh "ssh -o StrictHostKeyChecking=no ${ec2_instance} ${dockerComposeCmd}"

                        // shell script
                        sh "scp docker-compose.yaml ${ec2_instance}:/home/ec2-user"
                        sh "scp server-commands.sh ${ec2_instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2_instance} ${shellCmd}"

                    }
                }
            }
        }

        stage("commit version update") {
            steps {
                script {
                    // first - credentials id in Jenkins, second - where to push. Repo url, omitting the https protocol
                    gitLoginRemote "gitlab-credentials", "gitlab.com/saymolet/node-project.git"
                    // email and username for jenkins. Displayed with commit
                    gitConfig "jenkins@example.com", "jenkins"
                    // branch where to push and message with commit
                    gitAddCommitPush "master", "ci: version bump"
                }
            }
        }
    }
}