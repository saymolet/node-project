FROM node:16-alpine3.16
EXPOSE 3000

RUN mkdir -p usr/app
COPY app /usr/app

WORKDIR /usr/app
RUN npm install
CMD node server.js